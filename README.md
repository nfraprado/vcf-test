# Example vCard Network page

This repository hosts an example [vCard
Network](https://codeberg.org/nfraprado/vcard-network) page. See it live
[here](https://nfraprado.codeberg.page/vcf-test/main.html).
